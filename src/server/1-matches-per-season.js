const { Client } = require("pg");

const client = new Client(
  // Connection URL
  "postgres://lrferwxo:Ahn5QNKrJyfr2azhfDc5poxtBMxPAZzi@mel.db.elephantsql.com/lrferwxo"
);

client.connect(function (err) {
  if (err) {
    console.error("Error while connecting to the database " + err);
  } else {
    console.log("Connected Successfully");
    client.query(
      `SELECT season,count(season) AS matches_count 
      FROM matches 
      GROUP BY season 
      ORDER BY season;`,
      function (err, data) {
        if (err) {
          console.error("Error while fetching matches data " + err);
        } else {
          console.log(data.rows);
        }
        client.end();
      }
    );
  }
});
