const { Client } = require("pg");

const client = new Client(
  //connection link
  "postgres://lrferwxo:Ahn5QNKrJyfr2azhfDc5poxtBMxPAZzi@mel.db.elephantsql.com/lrferwxo"
);

client.connect(function (err) {
  if (err) {
    console.error("Error while connecting to database " + err);
  } else {
    console.log("Connected Successfully");
    client.query(
      `SELECT season,winner,count(winner) AS matches_won 
                    FROM matches
                    GROUP BY season,winner
                    ORDER BY season;`,
      function (err, data) {
        if (err) {
          console.error("Error while fetching data " + err);
        } else {
          console.log(data.rows);
        }
        client.end();
      }
    );
  }
});
