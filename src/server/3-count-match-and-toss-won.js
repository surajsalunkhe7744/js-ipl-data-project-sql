const { Client } = require("pg");

const client = new Client(
  //Connection URL
  "postgres://lrferwxo:Ahn5QNKrJyfr2azhfDc5poxtBMxPAZzi@mel.db.elephantsql.com/lrferwxo"
);

client.connect(function (err) {
  if (err) {
    console.error("Error while connecting to the database");
  } else {
    console.log("Connected Successfully");
    client.query(
      `SELECT winner as team_name,COUNT(winner) as wins_tosses_with_matches
        FROM matches
        WHERE toss_winner = winner
        GROUP BY winner;`,
      function (err, data) {
        if (err) {
          console.error("Error while fetching data " + err);
        } else {
          console.log(data.rows);
          client.end();
        }
      }
    );
  }
});
